delete from  taco_order_tacos;
delete from  taco_ingredients;
delete from  taco;
delete from  taco_order;
delete from ingredients;

insert into ingredients (id, name, type) values ('FLTO', 'Flour Tortilla', 'WRAP');
insert into ingredients (id, name, type) values ('COTO', 'Corn Tortilla', 'WRAP');
insert into ingredients (id, name, type) values ('GRBF', 'Ground Beef', 'PROTEIN');
insert into ingredients (id, name, type) values ('CARN', 'Carnitas', 'PROTEIN');
insert into ingredients (id, name, type) values ('TMTO', 'Decided Tomatoes', 'VEGGIES');
insert into ingredients (id, name, type) values ('LETC', 'Lettuce', 'VEGGIES');
insert into ingredients (id, name, type) values ('CHED', 'Cheddar', 'CHEESE');
insert into ingredients (id, name, type) values ('JACK', 'Monterrey Jack', 'CHEESE');
insert into ingredients (id, name, type) values ('SLSA', 'Salsa', 'SAUCE');
insert into ingredients (id, name, type) values ('SRCR', 'Sour Cream', 'SAUCE');