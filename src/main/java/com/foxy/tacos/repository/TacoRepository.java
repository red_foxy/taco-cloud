package com.foxy.tacos.repository;

import com.foxy.tacos.model.Taco;
import org.springframework.data.repository.CrudRepository;

/**
 * @author foxy
 * @since 10.06.19.
 */
public interface TacoRepository extends CrudRepository<Taco, Long> {
}
