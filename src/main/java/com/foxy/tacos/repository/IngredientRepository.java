package com.foxy.tacos.repository;

import com.foxy.tacos.model.Ingredient;
import org.springframework.data.repository.CrudRepository;

/**
 * @author foxy
 * @since 10.06.19.
 */
public interface IngredientRepository extends CrudRepository<Ingredient, Long> {
}
