package com.foxy.tacos.repository;

import com.foxy.tacos.model.Order;
import org.springframework.data.repository.CrudRepository;

/**
 * @author foxy
 * @since 10.06.19.
 */
public interface OrderRepository extends CrudRepository<Order, Long> {
}
