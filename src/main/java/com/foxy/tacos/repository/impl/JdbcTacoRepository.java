package com.foxy.tacos.repository.impl;

import com.foxy.tacos.model.Ingredient;
import com.foxy.tacos.model.Taco;
import com.foxy.tacos.repository.TacoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Types;
import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * @author foxy
 * @since 10.06.19.
 */
@Repository
public class JdbcTacoRepository implements TacoRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcTacoRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Taco save(Taco design) {
        long id = saveTacoInfo(design);
        design.setId(id);
        for (Ingredient ingredient : design.getIngredients()) {
            saveIngredientToTaco(ingredient, id);
        }
        return design;
    }

    private long saveTacoInfo(Taco taco) {
        taco.setCreatedAt(LocalDateTime.now());
        PreparedStatementCreator creator = new PreparedStatementCreatorFactory(
                "insert into taco (name, created_at) values (?,?)", Types.VARCHAR, Types.TIMESTAMP
        ).newPreparedStatementCreator(Arrays.asList(taco.getName(),
                taco.getCreatedAt()));

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(creator, keyHolder);
        return keyHolder.getKey().longValue();
    }

    private void saveIngredientToTaco(Ingredient ingredient, long id) {
        jdbcTemplate.update("insert into taco_ingredients (taco, ingredient) values (?,?)",
                id, ingredient.getId());
    }
}
